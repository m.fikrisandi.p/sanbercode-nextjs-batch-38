import React, { useEffect, useState } from 'react'
import axios from 'axios'
import Header from '../Header'

export default function index({ data }) {
    
    //state data
    const [dataContestants, setDataContestants] = useState(data)
    const [input, setInput] = useState({
        name: "",
        gender: "",
        height: ""
    })

    //indikator
    const [fetchStatus, setFetchStatus] = useState(false)
    const [currentId, setCurrentId] = useState(-1)

    let fetchData = async () => {
        let res = await axios.get(`https://backendexample.sanbercloud.com/api/student-scores`)
        let result = res.data
        setDataContestants([...result])
    }
    useEffect(() => {

        if (fetchStatus) {
            fetchData()
            setFetchStatus(false)
        }

    }, [fetchStatus, setFetchStatus])

    const handleSubmit = (event) => {
        event.preventDefault()

        let { name, gender, height } = input

        if (currentId === -1) {
            axios.post(`https://backendexample.sanbercloud.com/api/student-scores`, { name, course, score })
                .then((res) => {
                    setFetchStatus(true)
                })
        } else {
            axios.put(`https://backendexample.sanbercloud.com/api/student-scores/${currentId}`, { name, course, score })
                .then((res) => {
                    setFetchStatus(true)
                })
        }

        setInput(
            {
                name: "",
                course: "",
                score: ""
            }
        )

        setCurrentId(-1)

    }

    const handleDelete = (event) => {
        let idData = event.target.value

        axios.delete(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
            .then((res) => {
                console.log(res)
                setFetchStatus(true)
            })

    }

    const handleEdit = (event) => {
        let idData = event.target.value

        axios.get(`https://backendexample.sanbercloud.com/api/student-scores/${idData}`)
            .then((res) => {
                console.log(res)
                setCurrentId(res.data.id)

                setInput(
                    {
                        name: res.data.name,
                        gender: res.data.course,
                        height: res.data.score
                    }
                )

            })

    }

    const handleChange = (event) => setInput({ ...input, [event.target.name]: event.target.value })



    return (
        // <ul>
        //     {
        //         data !== undefined && data.map((res) => { return <li>{res.name}</li> })
        //     }
        // </ul>
        <>
            <Header/>
            <div className='w-1/2 mx-auto mt-20 text-center'>

                <div class="overflow-x-auto relative shadow-md sm:rounded-lg justify-center">
                    <table class="w-full text-sm text-left text-gray-500 dark:text-gray-400">
                        <thead class="text-xs text-gray-700 uppercase bg-gray-50 dark:bg-gray-700 dark:text-gray-400">
                            <tr>
                                <th scope="col" class="py-3 px-6">
                                    Nama
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Course
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Score
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Predikat
                                </th>
                                <th scope="col" class="py-3 px-6">
                                    Action
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            {
                                dataContestants !== undefined && dataContestants.map((res) => {
                                    return (
                                        <tr class="bg-white border-b dark:bg-gray-800 dark:border-gray-700 ">
                                            <th class="py-4 px-6 ">
                                                {res.name}
                                            </th>
                                            <td class="py-4 px-6">
                                                {res.course}
                                            </td>
                                            <td class="py-4 px-6">
                                                {res.score}
                                            </td>
                                            <td class="py-4 px-6">
                                                {setNilai(res.score)}
                                            </td>
                                            <td class="py-4 px-6">
                                                <button value={res.id} onClick={handleDelete} class="font-medium text-blue-600 dark:text-blue-500 hover:underline p-5">Delete</button>
                                                <button value={res.id} onClick={handleEdit} class="font-medium text-blue-600 dark:text-blue-500 hover:underline p-5">Edit</button>
                                            </td>
                                        </tr>
                                    )
                                })
                            }

                        </tbody>
                    </table>
                </div>


            </div>


            <form onSubmit={handleSubmit} className='w-1/2 mx-auto mt-5'>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Name</label>
                    <input value={input.name} onChange={handleChange} name='name' type="text" class="bg-gray-50 border border-gray-300 text-gray-900
                    text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="input name" required />
                </div>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Gender</label>
                    <input value={input.gender} onChange={handleChange} name='gender' type="text" class="bg-gray-50 border border-gray-300 text-gray-900
                    text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="input gender" required />
                </div>
                <div class="mb-6">
                    <label class="block mb-2 text-sm font-medium text-gray-900 dark:text-gray-300">Height</label>
                    <input value={input.height} onChange={handleChange} name='height' type="text" class="bg-gray-50 border border-gray-300 text-gray-900
                    text-sm rounded-lg focus:ring-blue-500 focus:border-blue-500 block w-full p-2.5 dark:bg-gray-700
                    dark:border-gray-600 dark:placeholder-gray-400 dark:text-white dark:focus:ring-blue-500 dark:focus:border-blue-500" placeholder="input height" required />
                </div>

                <button type="submit" class="text-white bg-blue-700 hover:bg-blue-800 focus:ring-4 focus:outline-none
                focus:ring-blue-300 font-medium rounded-lg text-sm w-full sm:w-auto px-5 py-2.5 text-center dark:bg-blue-600 dark:hover:bg-blue-700 dark:focus:ring-blue-800">Submit</button>
            </form>

        </>
    )
}


export async function getServerSideProps() {
    const res = await fetch(`https://backendexample.sanbercloud.com/api/student-scores/`)
    const data = await res.json()
    return {
        props: {
            data,
        },
    };
}

function setNilai(score) {
    let nilai = ''
    switch (true) {
        case score >= 80:
            nilai = 'A'
            break;
        case score >= 70 && score < 80:
            nilai = 'B'
            break;
        case score >= 60 && score < 70:
            nilai = 'C'
            break;
        case score >= 50 && score < 60:
            nilai = 'D'
            break;
        case score < 50:
            nilai = 'E'
            break;
    }
    return nilai
}
