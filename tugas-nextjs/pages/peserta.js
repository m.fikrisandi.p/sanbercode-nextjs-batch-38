import { Fragment } from "react";
import Header from './Header'

export default function Peserta() {
    return (
        <Fragment>
            <Header/>
            <div className="m-5">
                <h1 className="font-bold text-2xl mt-5 mb-5">
                    Data Peserta Sanbercode Bootcamp Nextjs
                </h1>
                <ol>
                    <li><b>Nama:</b> Muhammad Fikri Sandi Pratama</li>
                    <li><b>Email:</b> m.fikrisandi.p@gmail.com</li>
                    <li><b>Sistem Operasi Yang Digunakan:</b> Windows</li>
                    <li><b>Akun Gitlab:</b> Muhammad Fikri Sandi Pratama</li>
                    <li><b>Akun Telegram:</b> Fikri Sandi</li>
                    <li><b>Tanggal Lahir:</b> 20-08-2001 </li>
                    <li><b>Pengalaman Kerja:</b> Mahasiswa </li>
                </ol>
            </div>
        </Fragment>
    )
}