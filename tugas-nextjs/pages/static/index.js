import { Fragment } from 'react'
import Header from '../Header'
import { useRouter } from 'next/router'
import styles from '../../styles/static.module.css'

export default function UserDetail(props) {
  const { data } = props;
  const router = useRouter();
  console.log(data);
  return (
    <Fragment>
      <Header />
      <div className="flex flex-wrap max-w-5xl m-auto items-center justify-center">

        {data.map((user) => (
          <div key={user.id} onClick={() => router.push(`/static/${user.id}`)} className={styles.card}>
            <p className="mb-8 font-semibold">
              Kartu Mahasiswa
            </p>
            <hr />
            <p className="font-bold mt-5 cursor-pointer">{user.name}</p>
            <p className="pt-5">Klik untuk melihat detail</p>
          </div>
        ))}
      </div>

    </Fragment>
  );
}

export async function getStaticProps() {
  const result = await fetch('https://backendexample.sanbercloud.com/api/student-scores')
  const data = await result.json()

  return {
    props: { data, },
  };
}