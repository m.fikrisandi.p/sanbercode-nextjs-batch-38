import Header from '../Header'
import {Fragment} from 'react'

export default function userDetail(props) {
    const { user } = props;
    return (
        <Fragment>
            <Header />
            <div>
                <h1 className="font-bold text-2xl mb-5">Detail Data</h1>
                <ol className="list-decimal">
                    <li><p>Nama : {user.name}</p></li>
                    <li><p>Course : {user.course}</p></li>
                    <li><p> Score : {user.score}</p></li>
                    <li><p>Predikat : {setNilai(user.score)}</p></li>
                </ol>
            </div>
        </Fragment>
    )
}

export async function getStaticPaths() {
    const result = await fetch('https://backendexample.sanbercloud.com/api/student-scores')
    const data = await result.json()
    const paths = data.map((user) => ({
        params: {
            id: `${user.id}`,
        },
    }));
    return {
        paths, fallback: false,
    };
}

export async function getStaticProps(item) {
    const { id } = item.params;
    const result = await fetch(`https://backendexample.sanbercloud.com/api/student-scores/${id}`)
    const user = await result.json()

    return {
        props: {
            user,
        },
    };
}

function setNilai(score) {
    let nilai = ''
    switch (true) {
        case score >= 80:
            nilai = 'A'
            break;
        case score >= 70 && score < 80:
            nilai = 'B'
            break;
        case score >= 60 && score < 70:
            nilai = 'C'
            break;
        case score >= 50 && score < 60:
            nilai = 'D'
            break;
        case score < 50:
            nilai = 'E'
            break;
    }
    return nilai
}
