import { Fragment } from 'react'
import { Navbar } from 'flowbite-react'
import Image from 'next/image'

export default function index() {
    return (
        <Fragment>
            <Navbar
                fluid={true}
                rounded={true}
                style={{ padding: '20px 30px' }}
                className="bg-sky-500"

            >
                <Navbar.Brand href="/">
                    <Image
                        src={'/../public/logo.png'}
                        className="mr-3 h-6 sm:h-9"
                        alt="Sanbercode Logo"
                        width="100"
                        height="50"
                    />
                    <span className="m-0 h-fit self-center">
                        | NextJS
                    </span>
                </Navbar.Brand>
                <Navbar.Toggle />
                <Navbar.Collapse>
                    <Navbar.Link
                        href="/"
                        active={true}
                    >
                        Home
                    </Navbar.Link>
                    <Navbar.Link href="/Peserta">
                        Peserta
                    </Navbar.Link>
                    <Navbar.Link href="/Data">
                        Data
                    </Navbar.Link>
                </Navbar.Collapse>
            </Navbar>
        </Fragment>
    )
}
