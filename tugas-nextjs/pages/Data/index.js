import { Fragment } from 'react'
import Header from '../Header'
import Link from 'next/Link'


export default function index() {
    return (
        <Fragment>
            <Header />
            <div className="flex justify-around overflow-0">
                <div className="flex h-screen w-full justify-center items-center bg-red-500">
                    <Link href="/static">
                        <a className="h-fit text-black text-2xl font-semibold" >
                            Static
                        </a>
                    </Link>
                </div>
                <div className="flex h-screen w-full justify-center items-center bg-blue-500">

                    <Link href="/server">
                        <a className="h-fit text-black text-2xl font-semibold" >
                            server
                        </a>
                    </Link>
                </div>    
            </div>


        </Fragment>
    )
}
